// Objects and Arrays

// Number of duplicates

function addToObj(obj, key, value) {
    obj[key] = value;
}

function numberOfDuplicates(arr) {
  let occurrence = {};
  for (let i = 0; i < arr.length; ++i) {
      if (occurrence[arr[i]] === undefined) {
        addToObj(occurrence, arr[i], 1);
        arr[i] = 1;
      } else { 
        occurrence[arr[i]] +=1;
          arr[i] = occurrence[arr[i]]
        }
  }
  return arr
}

// console.log(numberOfDuplicates([1, 2, 1, 1, 3])); // [1, 1, 2, 3, 1]
// console.log(numberOfDuplicates(['a', 'a', 'aa', 'a', 'aa'])); // [1, 2, 1, 3, 2]




// Object strength

function countObjectStrength(arr) {
    let strength = 0;
    if (arr.length === 0) {
        strength = 2;
    } else {
        for (let i = 0; i < arr.length; ++i) {
            switch(typeof arr[i]) {
                case 'undefined': 
                    strength += 0;
                case 'boolean':
                    strength += 1;
                case 'number': 
                    strength += 2;
                case 'string': 
                    strength += 3;
                case 'object': 
                    strength += 5;
                case 'function': 
                    strength += 7;
                case 'bigint': 
                    strength += 9;
                case 'symbol': 
                    strength += 10;
                
            }
        }
    }
    
    return strength
}

// let arr = [true];
// for (let item of arr) {
//     console.log(typeof item);
// }
// console.log(arr);
// console.log(countObjectStrength(arr));





// Scope

function multiply(num1) {
  return function(num2) {
    return function(num3) {
      return num1 * num2 * num3
    }
  }
}
// console.log(multiply(2)(4)(6)); // 48
// console.log(multiply(3)(3)(3)); // 27 )


function createCounter(count) {
  
    function getCounter() {
      alert(getCounter.count++);
    };
  
    getCounter.count = count + 1;
  
    return getCounter;
  }
  
//   let getCounter = createCounter();
//   const counter = createCounter(44);
// counter(); // 45
// counter(); // 46
// counter(); // 47





// Classes

class Task {
    constructor(name) {
        this.name = name;
    }
}

class Guest {
  constructor(tasks) {
    this.tasks = tasks;
  }
  getTask(id) {
    return this.tasks[id];
  }
  createTask() {}
  changeType() {}
}

class User extends Guest {
  constructor (tasks, getTask, changeType) {
    super(tasks, getTask, changeType);
  }
  createTask(task) {
    this.tasks.push(task);
  }
  
}

class Admin {
  constructor(userArr) {
      this.userArr = userArr;
  }
  getArray() {
      return this.userArr
  }
  changeType(id) {
    if (this.userArr[id] instanceof User) {
        this.userArr[id] = new Guest;
    } else { this.userArr[id] = new User }
  }


}

// const guest = new Guest(
//     [
//       new Task('task name 1'),
//       new Task('task name 2'),
//       new Task('task name 3'),
//     ]
//   );
  
//   console.log(guest.getTask(0)); // { name: 'task name 1' }
//   console.log(guest.getTask(2)); // { name: 'task name 3' }
//   guest.createTask(new Task('task name 4')); // taskName is not defined, should not work
  
//   const user = new User(
//     [
//       new Task('task name 1'),
//       new Task('task name 2'),
//       new Task('task name 3'),
//     ]
//   );
  
//   console.log(user.getTask(0)); // { name: 'task name 1' }
//   console.log(user.getTask(2)); // { name: 'task name 3' }
//   user.createTask(new Task('task name 4'));
//   console.log(user.getTask(3)); // {name: 'task name 4'}

//   const admin = new Admin(
//     [
//       new Guest([]),
//       new Guest([new Task('task name 1')]),
//       new User([]),
//       new User([new Task('task name 2')]),
//     ]
//   );

//   console.log(admin.getArray()); // [Guest, Guest, User, User]
//   admin.changeType(1);
//   console.log(admin.getArray()); // [Guest, User, User, User]



// Prototypes



function Logger() {
    this.logArr = []
   };

   Logger.prototype.log = function(str) {
       this.logArr.push(str)
   };
   Logger.prototype.getLog = function() {
        return this.logArr
    };
    Logger.prototype.clearLog = function() {
        this.logArr.splice(0, this.logArr.length);
    };

   
//    const logger = new Logger();
//    logger.log('Event 1');
//    logger.log('Event 2');
//    console.log(logger.getLog()); // ['Event 1', 'Event 2']
//    logger.clearLog();
//    console.log(logger.getLog()); 


Array.prototype.shuffle = function() {
    let arrLength = this.length;
    let randNum;
    let temp;
    if ( arrLength == 0 ) return this;
    while ( --arrLength ) {
       randNum = Math.floor( Math.random() * ( arrLength + 1 ) );
       temp = this[arrLength];
       this[arrLength] = this[randNum];
       this[randNum] = temp;
    }
    return this;
  }

//   console.log([1, 2, 3, 4, 5, 6].shuffle());




// Async



// Strategies
function callback1(arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; ++i) {
        if (typeof arr[i] === 'number') {
            sum += arr[i];
        } else { continue }
    }
    return sum
}

function callback2(arr) {
    let prod = 1;
    for (let i = 0; i < arr.length; ++i) {
        if (typeof arr[i] === 'number') {
            prod *= arr[i];
        } else { continue }
    }
    return prod
}

function w(str, callback) {
    let arr = str.split(' ');
    for (let i = 0; i < arr.length; ++i) {
        arr[i] = arr[i].length;
    }
    return callback(arr)
}


// console.log(w('a bb ccc dddd', callback1)); // result: 10
// console.log(w('a bb ccc dddd', callback2)); // result: 24




//  Mocker 
function mocker(arr) {
   return new Promise(function(resolve) {
        setTimeout(() => resolve(arr), 1000);
      });
} 

// const getUsers = mocker([{id: 1, name: 'User1'}]);
// getUsers.then((users) => { // Will fire after 1 second.
//   console.log(users); // result: [{id: 1, name: 'User1'}];
// });


// Summarize1
// Create the function summarize1 that receives promises and returns promise with sum of their values as shown below:
function summarize1(prom1, prom2) {
    let sum;
  return Promise.all(
        [prom1, prom2]
    ).then (
       data => {
           sum = data[0] + data[1]
           return sum
       }
    )
}


// const promise1 = Promise.resolve(4);
// const promise2 = new Promise((resolve) => resolve(2));

// summarize1(promise1, promise2).then(data => console.log( data))





// Summarize2
// Create the async function summarize2 that receives promises and returns promise with sum of their values as shown below:

async function summarize2(prom1, prom2) {
    let res = await Promise.all(
        [prom1, prom2]
    ).then (
    data => {
        sum = data[0] + data[1]
        return sum
    }
    )
    return res;
}


// const promise1 = Promise.resolve(4);
// const promise2 = new Promise((resolve) => resolve(2));

// summarize2(promise1, promise2).then((sum) => {console.log(sum);}); // result: 6





// ES6


// Is valid JSON
function isValidJSON(str) {
    try {
        if (typeof JSON.parse(str) === 'object') {
                return true
            }
    }
    catch(error) {
        return false
    }
}
// console.log(isValidJSON('{"a": 2}')); // result: true;
// console.log(isValidJSON('{"a: 2')); // result: false;





// Greeting

function greeting(obj) {
    return `Hello, my name is ${obj.name} ${obj.surname}, and I am ${obj.age} years old.`
}

// console.log(greeting({name: 'Bill', surname: 'Jacobson', age: 33})); // result: Hello, my name is Bill Jacobson and I am 33 years old.
// console.log(greeting({name: 'Jim', surname: 'Power', age: 11})); // result: Hello, my name is Jim Power and I am 11 years old.


// Unique Numbers

function unique(arr) {
    uniqueSet = new Set();
    for (let i = 0; i < arr.length; ++i) {
        uniqueSet.add(arr[i]);
    }
    arr = Array.from(uniqueSet);
    return arr;
}

// console.log(unique([1, 1, 2, 3, 5, 4, 5])); // result: [1, 2, 3, 5, 4]



// Lego Generator
function* generator(arr) {
    for (let i = 0; i < arr.length; ++i) {
        yield arr[i];
    }
}
// const it = generator(['brick', 'plate', 'minifigure', 'slope']);
// console.log(it.next().value); // return 'brick'
// console.log(it.next().value); // return 'plate'